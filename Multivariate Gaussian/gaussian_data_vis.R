rm(list = ls())
graphics.off()
library(data.table)
data = read.csv('multi_variate_gaussian.csv', stringsAsFactors = T)
data = as.data.frame(data)
names(data) <- c( "v1","v2","v3", "v4", "v5", "class")

pairs(data[1:5], main = "Scatter : Multi-variate Gaussian Data", 
      pch = 21, 
      bg = c("blue","orange", "green","red")[unclass(data$class)]
      )
