The project contains the R-codes for running 

1. PVM, 
2. K-Means, 
3. Spectral Clustering 
4. Mean Shift Algorithm

There are 5 data sets

1. Gaussian Mixture
2. Solid Sphere
3. Concentric Sphere
4. Iris
5. Pima Indian Diabetes. 

Each of the folder captures the codes for different dataset.

Inside each folder, there are codes to run different algorithm.

