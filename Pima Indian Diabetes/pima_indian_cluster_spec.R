## Spectral_Clustering Comparison Script 
## Author Sambarta Dasgupta

rm(list = ls())
## Start Time
ptm <- proc.time()

library('kernlab')
iter = 100

load('PimaIndiansDiabetes.rda')
PimaIndiansDiabetes <- as.data.frame(PimaIndiansDiabetes)
PimaIndiansDiabetes = PimaIndiansDiabetes[,which(apply(PimaIndiansDiabetes, 2, anyNA)==FALSE)]

X <- as.matrix(PimaIndiansDiabetes[,1:(ncol(PimaIndiansDiabetes)-1)])

X <- apply(X,1,function(x){as.numeric(x)})
names(X) <- NULL

f_score_vec = error_vec = rep(0,iter)
for(i in 1:iter){
kkmeans_sol <- kkmeans(t(X),centers=2)

estimated_cluster = paste('Spec',as.vector(kkmeans_sol),sep="_")
actual_cluster = PimaIndiansDiabetes$diabetes

confusion_matrix = table( actual_cluster, estimated_cluster)

row.max <- apply(confusion_matrix,1,which.max)
confusion_matrix = confusion_matrix[names(sort(row.max)),]

error = sum(confusion_matrix)-sum(diag(confusion_matrix))
error_vec[i] = error

f_score=rep(0,nrow(confusion_matrix))
for (k in 1:nrow(confusion_matrix)){
  if(k<=ncol(confusion_matrix)){
    precision = confusion_matrix[k,k]/sum(confusion_matrix[k,])
    recall = confusion_matrix[k,k]/sum(confusion_matrix)
    if(precision+recall>0){
      f_score[k] = 2*(precision*recall)/(precision+recall)
    } else {
      f_score[k] = 0
    }
  }
}
f_score_vec[i] = mean(f_score)

if(i==1){
  confusion_matrix_best= confusion_matrix
  error_min = error
} else if (error_min > error){
  confusion_matrix_best= confusion_matrix
  error_min = error
}

}

confusion_matrix_best
mean(error_vec)
min(error_vec)
sd(error_vec)

mean(f_score_vec)
sd(f_score_vec)

## End time
proc.time() - ptm